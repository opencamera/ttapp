import 'package:flutter/material.dart';

class LoginOptionsPage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          padding: EdgeInsets.only(top: 150.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 37),
                child: TextGroup()
              ),
              Padding(
                padding: EdgeInsets.only(left: 37),
                child: ButtonGroup()
              ),
              Padding(
                padding: EdgeInsets.only(left: 37, top: 20),
                child:  InkWell(
                onTap: () {Navigator.pushNamed(context, "/signin");},
                child: Text(
                  "Don't have an account? Sign up",
                  style: TextStyle(
                    color: Color(0xffee796b)
                  )
                )
              ),
              )
            ]
          )
        )
      )
    );
  }
}

class TextGroup extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Padding(
      padding: EdgeInsets.only(top: 30.0),
      child: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10, top: 30),
              child: Text(
                'Welcome to thousandthread.',
                style: TextStyle(
                  fontSize: 32,
                  fontFamily: 'Futura',
                  fontWeight: FontWeight.bold
                )
              )
            ),
            Text(
              'Join the community of threaders and get',
              style: TextStyle(
                color: Color(0xffaaaaaa),
              )
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 40),
              child: Text(
                'access to hundreds of closets around you.',
                style: TextStyle(
                  color: Color(0xffaaaaaa),
                )
              )
            ),
          ],
        )
      )
    );
  }
}

class ButtonGroup extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 20),
          child: UserNameButton()
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 20),
          child: FacebookButton()
        ),
        GoogleButton()
      ],
    );
  }
}

class UserNameButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: 45,
      child: FlatButton(
        color: Color(0xffee796b),
        onPressed: () => Navigator.pushNamed(context, '/signin'),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 25),
              child: Image.asset('images/login/loginPerson.png')
            ),
            Text(
              'Continue with Username or Email',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w300
              )
            )
          ],
        )
      )
    );  
  }
}

class FacebookButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: 45,
      child: FlatButton(
        color: Color(0xff3b5998),
        onPressed: () => print("hello"),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 25, left: 4),
              child: Image.asset('images/login/facebooklogo.png')
            ),
            Padding(
              padding: EdgeInsets.only(left: 30),
              child: Text(
                'Continue with Facebook',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w300
                )
              )
            ),
          ],
        )
      )
    );  
  }
}

class GoogleButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: 45,
      child: OutlineButton(
        borderSide: BorderSide(
          color: Colors.black
        ),
        onPressed: () => print("hello"),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 24, left: 1),
              child: Image.asset('images/login/googlelogo.png')
            ),
            Padding(
              padding: EdgeInsets.only(left: 30),
              child: Text(
                'Continue with Google',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400
                )
              )
            ),
          ],
        )
      )
    );  
  }
}