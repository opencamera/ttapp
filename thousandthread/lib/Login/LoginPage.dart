import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          padding: EdgeInsets.only(top: 150.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Log in to continue',
                style: TextStyle(
                  fontFamily: 'Futura',
                  color: Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.bold
                )
              ),
              LoginForm()
            ]
          )
        )
      )
    );
  }
}

class LoginForm extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Padding(
      padding: EdgeInsets.only(top: 30.0),
      child: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Text(
                'Username or email',
                style: TextStyle(
                  fontFamily: 'Futura',
                  fontWeight: FontWeight.w600
                )
              ),
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: SizedBox(
                width: 250.0,
                height: 35.0,
                child: TextField(
                    decoration: InputDecoration(
                      focusColor: Colors.blue,
                      focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 0,
                        color: Color(0xffee796b)
                      ),
                    ),
                      contentPadding: const EdgeInsets.only(bottom: 20.0, left: 10.0),
                      border: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(3.0),
                        )
                      ),
                    filled: true,
                    fillColor: Color(0xffededed),
                  )
                )
              ),
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child: Text(
                'Password',
                style: TextStyle(
                  fontFamily: 'Futura',
                  fontWeight: FontWeight.w600
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: SizedBox(
                width: 250.0,
                height: 35.0,
                child: TextField(
                  decoration: InputDecoration(
                    focusColor: Colors.black,
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 0,
                        color: Color(0xffee796b)
                      ),
                    ),
                    contentPadding: const EdgeInsets.only(bottom: 20.0, left: 10.0),
                    border: 
                    OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 100.0,
                      ),
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(3.0),
                      )
                    ) ,
                    filled: true,
                    fillColor: Color(0xffededed),
                  )
                )
              )
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 30.0),
              child: Text(
                'Forgot password?',
                style: TextStyle(
                  color: Color(0xffee796b)
                )
              ),
            ),
        
            SizedBox(
              width: 250.0,
              child: FlatButton(
                onPressed: () =>{},
                color: Color(0xffededed),
                child: Text(
                  'Log in',
                  style: TextStyle(
                    color: Color(0xffaaaaaa)
                  )
                )
              )
            )
          ],
        )
      )
    );
  }
}