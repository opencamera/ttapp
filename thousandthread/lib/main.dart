import 'package:flutter/material.dart';
import 'package:thousandthread/Login/LoginPage.dart';
import 'package:thousandthread/Login/LoginOptionsPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      initialRoute: '/',
      routes:{
        '/': (context) => LoginOptionsPage(),
        '/signin': (context) => LoginPage(),
      }
    );
  }
}